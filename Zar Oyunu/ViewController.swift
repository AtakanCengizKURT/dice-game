//
//  ViewController.swift
//  Zar Oyunu
//
//  Created by Atakan Cengiz KURT on 27.01.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var imgFirstDice: UIImageView!
    @IBOutlet weak var imgSecondDice: UIImageView!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var lblRound: UILabel!
    @IBOutlet weak var btnPlayDice: UIButton!
    
    var timer = Timer()
    var randomNumber = 1
    var randomNumber2 = 1
    var isTouchPlayDice: Bool = false
    var totalScore: Int = 0
    var round = 0
    var roundDice = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblScore.text = "0"
        lblRound.text = "0"
    }
    
    @IBAction func action_playDice(_ sender: Any) {
        
        playDice()
        
    }
    
    @objc func changeDice(){
        if roundDice < 10{
        roundDice += 1
        randomNumber = Int.random(in: 1...6)
        randomNumber2 = Int.random(in: 1...6)
        imgFirstDice.image = UIImage(named: "Group_\(randomNumber)")
        imgSecondDice.image = UIImage(named: "Group_\(randomNumber2)")
        }else{
        calculateScore()
        }
    }
    
    
    func calculateScore(){
        roundDice = 0
        isTouchPlayDice = false
        timer.invalidate()
        totalScore += randomNumber * randomNumber2
        lblScore.text = "\(totalScore)"
        btnPlayDice.isEnabled = true
    }
    
    
    func playDice(){
        btnPlayDice.isEnabled = false
        round += 1
        lblRound.text = "\(round)"
        isTouchPlayDice = true
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(changeDice), userInfo: nil, repeats: true)
    }
    
    
    @IBAction func action_slider(_ sender: Any) {
        
    }
    
    @IBAction func action_info(_ sender: Any) {
        
        
    }
    
    @IBAction func action_restart(_ sender: Any) {
        let alert = UIAlertController(title: "Oyun Bitti", message: "Tebrikler Puanınız: \(totalScore)", preferredStyle: .alert)
        let done = UIAlertAction(title: "Tamam", style: .default) { (action) in
        self.startNewGame()
        }
        alert.addAction(done)
        self.present(alert, animated: true)
    }
    
    func startNewGame(){
        lblScore.text = "0"
        lblRound.text = "0"
        randomNumber = 1
        randomNumber2 = 1
        imgFirstDice.image = UIImage(named: "Group_\(randomNumber)")
        imgSecondDice.image = UIImage(named: "Group_\(randomNumber2)")
        isTouchPlayDice = false
        totalScore = 0
        round = 0
        
        timer.invalidate()
    }
    
}

