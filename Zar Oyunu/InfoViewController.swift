//
//  InfoViewController.swift
//  Zar Oyunu
//
//  Created by Atakan Cengiz KURT on 27.01.2019.
//  Copyright © 2019 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textView.clipsToBounds = true
        textView.layer.cornerRadius = 50
        textView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMinYCorner]
    }
    
    @IBAction func action_close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
